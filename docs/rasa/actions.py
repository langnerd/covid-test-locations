##################################################################################
#  You can add your actions in this file or create any other file in this folder #
##################################################################################

from rasa_sdk import Action
from rasa_sdk.events import SlotSet, ReminderScheduled, ConversationPaused, ConversationResumed, FollowupAction, \
    Restarted, ReminderScheduled
import requests


class MyAction(Action):

    def name(self):
        return 'action_my_action'

    def run(self, dispatcher, tracker, domain):
        # do something.
        return []


class GetLocationsUSAction(Action):

    def name(self):
        return 'action_get_locations_us'

    def run(self, dispatcher, tracker, domain):
        country = next(tracker.get_latest_entity_values("country"), None)
        if country:
            url = 'https://covid19-test-locations.herokuapp.com/locations/us/{}?display_format=flat'.format(country)
            data = requests.get(url).json()
            message = '\n\n'.join([str(elem) for elem in data])
            dispatcher.utter_message(message)
        return []
