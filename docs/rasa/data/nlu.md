# lang:en

# canonical
- arizona
- us_west

## intent:get_locations_us
- arizona
- nevada
- california

## intent:us_west
- us_west

## intent:us_northeast
- us_northeast

## intent:choose_country
- utah
- washington
- delaware
- florida
- texas
- new_jersey
- new_york
- pennsylvania
- massachusetts

## intent:us_south
- us_south

## intent:us_midwest
- us_midwest
