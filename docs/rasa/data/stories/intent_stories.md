# Intent stories

## US West
* us_west
  - utter_us_west
## US Midwest
* us_midwest
  - utter_us_midwest
## US South
* us_south
  - utter_us_south
## US Northeast
* us_northeast
  - utter_us_northeast
## Choose country
* choose_country
  - action_get_locations_us