# COVID-19 Test Locations

![Demo Video](docs/assets/covid19-rasabot.mov)

Configuration:
- [config](docs/rasa/config.yml)
- [domain](docs/rasa/domain.yml)

Intents and entities:
- [nlu](docs/rasa/data/nlu.md)
- [stories](docs/rasa/data/stories)

Actions:
- [actions](docs/rasa/actions.py)

[Check the API](https://gitlab.com/langnerd/covid-test-locations/README.md) to see what's possible and kindly let us know your feedback.

Have fun :)

The project was created by using <a href="https://botfront.io/rasa" target="_blank">botfront</a>.