import sbt._

object Dependencies {

  val Http4sVersion = "0.21.2"
  val CirceVersion = "0.13.0"
  val Specs2Version = "4.8.3"
  val LogbackVersion = "1.2.3"
  val ScalaScraperVersion = "2.2.0"

  object Circe {
    val generic = "io.circe" %% "circe-generic" % CirceVersion
    val literal = "io.circe" %% "circe-literal" % CirceVersion
  }

  object Common {
    val specs2 = "org.specs2" %% "specs2-core" % Specs2Version % "test"
    val logback = "ch.qos.logback" % "logback-classic" % LogbackVersion
    val scalaScraper = "net.ruippeixotog" %% "scala-scraper" % ScalaScraperVersion
  }

  object Http4s {
    val circe = "org.http4s" %% "http4s-circe" % Http4sVersion
    val client = "org.http4s" %% "http4s-blaze-client" % Http4sVersion
    val dsl = "org.http4s" %% "http4s-dsl" % Http4sVersion
    val server = "org.http4s" %% "http4s-blaze-server" % Http4sVersion
  }
}
