# COVID-19 Test Locations

The purpose of this project is to provide access to information about medical
centres around the globe where you can get tested for COVID-19.

See <a href="https://www.langnerd.com/covid-19-test-locations/" target="_blank">this blog post</a> for details about the idea behind the project.


## How Does it Work

Currently the lookup works for all of the US states and covers many cities. The aim is to extract key information
about each of the found test centres. This includes physical address, opening times, contact details and essential instructions.

Play this video to get a picture of what to expect:

![Demo Video](docs/assets/covid19-snatchbot.mp4)

If you like the chatbot, please go ahead and paste this code snippet into your own website:

```
 <script src="https://account.snatchbot.me/script.js"></script><script>window.sntchChat.Init(100062)</script> 
```

If you are a fan of <a href="https://rasa.com/" target="_blank">Rasa</a>, go grab [all the resources you need](docs/rasa/README.md) and tweak the template bot to your needs.

## Data Sources
I don't collect any data on my own. The API rather relies on data sets
generously provided by someone else. Please find below a list of data providers.

- <a href="https://covid-19-apis.postman.com/covid-19-testing-locations/" target="_blank">Postman</a>
- <a href="https://my.castlighthealth.com/corona-virus-testing-sites/" target="_blank">Castlight</a>

## How to Run the Project Locally

Clone this repo and run `sbt run`

## Endpoints

The server runs at `https://covid19-test-locations.herokuapp.com`.

### Locations per Country and Region

`GET locations/us/{COUNTRY}/{REGION}`

Example:

```
curl \
--request GET \
'https://covid19-test-locations.herokuapp.com/locations/us/nj/bergen' | jq
```
yields

```
[
 {
    "id": {
      "value": "1"
    },
    "name": "Eatontown Urgent Care",
    "description": "Central Jersey Urgent Care is testing daily ...",
    "addresses": [
      {
        "id": {
          "value": "1"
        },
        "street": "142 Route 35",
        "city": "Eatontown",
        "county": "Region",
        "region": "New Jersey",
        "postalCode": "  07724",
        "country": "US"
      }
    ],
    "phones": [
      {
        "number": " +1 732-515-5111",
        "extension": "None"
      }
    ],
    "schedule": [
      {
        "weekDay": {
          "ordinal": 1
        },
        "opensAt": "9:00 AM",
        "closesAt": "5:00 PM"
      },
      {
        "weekDay": {
          "ordinal": 2
        },
        "opensAt": "9:00 AM",
        "closesAt": "5:00 PM"
      },
      ...
    ]
  },
  {
    "id": {
      "value": "2"
    },
    "name": "Marlboro Urgent Care",
]
```


### Locations per Country and Region in a Concise Format

`GET locations/us/{COUNTRY}/{REGION}?display_format=flat`

Example:

```
curl \
--request GET \
'https://covid19-test-locations.herokuapp.com/locations/us/nj/bergen?display_format=flat' | jq
```

yields

```
[
    "Eatontown Urgent Care | 142 Route 35 - Eatontown -   07724 | +1 732-515-5111",
    "Marlboro Urgent Care | 167 Route 9 - Marlboro -   07751 | +1 732-360-5799",
    "Howell Urgent Care | 4564 Route 9 - Howell -   07731 | +1 732-335-0720",
    "PNC Bank Arts Center | 116 Garden State Pkwy - Holmdel -   07733 | +1 (732) 203-2500",
    "Kean University | 1000 Morris Ave - Union -   07083 | +1 (732) 557-8000",
    "Paramus Bergen Community College | Bergen Community College, Paramus Campus – Lots B & C, 400 Paramus Road - Paramas -  07652 | +1 (201) 447-7100",
    "Camden County College | 200 College Dr - Blackwood -  08012 | +1 (856) 227-7200",
    "Hudson Regional Hospital | 55 Meadowlands Pkwy - Secaucus - 07094 | +1 201-388-1097",
    "Riverside Medical Group/Secaucus | 714 Tenth Street - Secaucus -  07094 | +1 201-863-3346",
    "PNC Bank Arts Center | 116 Garden State Pkwy - Holmdel -  07733 | +1 201-388-1097"
]
```

### Locations per Country and Region as Rich Cards

This endpoint is specifically for an integration with <a href="https://support.snatchbot.me/docs/card-types" target="_blank">Snatchbot</a>.

`POST locations/us/search`

#### Search by Country and Region

```
curl --request POST 'https://covid19-test-locations.herokuapp.com/locations/us/search' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'bot_id=123' \
--data-urlencode 'user_id=1' \
--data-urlencode 'module_id=1' \
--data-urlencode 'country=Delaware' \
--data-urlencode 'incoming_message=Sussex'
```

#### Search by Address

```
curl --request POST 'https://covid19-test-locations.herokuapp.com/locations/us/search?by=address' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'bot_id=123' \
--data-urlencode 'user_id=1' \
--data-urlencode 'module_id=1' \
--data-urlencode 'address=address=49+St+Subway+Station%2C+New+York%2C+NY+10019%2C+USA%2C+lat%3A+40.759011%2C+lng%3A+-73.9844722&bot_id=100062'
```
