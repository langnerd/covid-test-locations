import sbt.Keys.name

val commonSettings = Seq(
  organization := "com.langnerd",
  name := "covidtestlocations",
  version := "0.0.1-SNAPSHOT",
  scalaVersion := "2.13.1"
)

lazy val root = (project in file("."))
  .enablePlugins(JavaAppPackaging)
  .settings(commonSettings: _*)
  .settings(
    libraryDependencies ++= Seq(
      Dependencies.Circe.generic,
      Dependencies.Circe.literal,
      Dependencies.Common.logback,
      Dependencies.Common.scalaScraper,
      Dependencies.Common.specs2,
      Dependencies.Http4s.circe,
      Dependencies.Http4s.client,
      Dependencies.Http4s.dsl,
      Dependencies.Http4s.server
    ),
    addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.10.3"),
    addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.0")
  )

scalacOptions ++= Seq(
  "-deprecation",
  "-encoding",
  "UTF-8",
  "-language:higherKinds",
  "-language:postfixOps",
  "-feature",
  "-Xfatal-warnings",
)
