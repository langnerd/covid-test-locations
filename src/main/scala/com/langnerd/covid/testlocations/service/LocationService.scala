package com.langnerd.covid.testlocations.service

import cats.Monad
import cats.effect.Sync
import cats.implicits._
import com.langnerd.covid.testlocations.datasource.{
  CountryProvider,
  CountyProvider
}
import com.langnerd.covid.testlocations.model._
import com.langnerd.covid.testlocations.model.core.{RegionQuery, CountyQuery}

trait LocationService[F[_]] {
  def get(region: String): F[List[Location]]
  def get(region: String, county: String): F[List[Location]]
}

object LocationService {

  def apply[F[_]](implicit ev: LocationService[F]): LocationService[F] = ev

  def impl[F[_]: Sync](countyProvider: CountyProvider[F],
                       countryProvider: CountryProvider[F])(
      implicit F: Monad[F]): LocationService[F] = new LocationService[F] {
    override def get(region: String): F[List[Location]] =
      for {
        result <- countryProvider.find(RegionQuery(region))
      } yield result

    override def get(region: String, county: String): F[List[Location]] =
      for {
        counties <- countyProvider.find(
          CountyQuery(regionCode = region, county = county))
        result <- if (counties.isEmpty)
          countryProvider.find(RegionQuery(region))
        else F.pure(counties)
      } yield result
  }

}
