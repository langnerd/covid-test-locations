package com.langnerd.covid.testlocations.service

import java.nio.charset.Charset

import cats._
import cats.effect.Sync
import cats.implicits._
import com.langnerd.covid.testlocations.api.snatchbot.ConversationId
import com.langnerd.covid.testlocations.model.core.{Constants, RegionCodes}
import com.langnerd.covid.testlocations.model.snatchbot.{
  Button,
  CardResponse,
  GalleryCard,
  GalleryItem
}
import com.langnerd.covid.testlocations.model.{DailySchedule, Location, Phone}
import org.http4s.Uri

trait SnatchBotService[F[_]] {
  def cards(conversationId: ConversationId,
            regioCode: String,
            locations: List[Location]): F[CardResponse]
}

object SnatchBotService {

  def apply[F[_]](implicit ev: SnatchBotService[F]): SnatchBotService[F] = ev

  def impl[F[_]: Sync](apiKey: String)(
      implicit F: Applicative[F]): SnatchBotService[F] =
    new SnatchBotService[F] {
      override def cards(conversationId: ConversationId,
                         regionCode: String,
                         locations: List[Location]): F[CardResponse] = {
        val region =
          RegionCodes.US.find(regionCode).getOrElse(Constants.Strings.Empty)
        F.pure(
          CardResponse(
            conversationId.user_id,
            conversationId.bot_id,
            conversationId.module_id,
            s"COVID-19 test locations in $region.\nUse the arrows to see all options.",
            galleryCards(locations)
          )
        )
      }

      private def galleryCards(locations: List[Location]): List[GalleryCard] =
        List(GalleryCard("gallery", "gallery", locations.map(galleryItem)))

      private def galleryItem(location: Location): GalleryItem = {
        val placeUrl = googleMapLink(location)
        GalleryItem(
          googleMapPreviewUrl(location),
          header(location),
          schedule(location.schedule),
          placeUrl,
          buttons(location.phones, placeUrl, drivingDirectionsLink(location))
        )
      }

      private def encodedAddress(location: Location): Option[String] =
        location.addresses.headOption.map { address =>
          Uri.encode(s"${location.name},${address.street},${address.city}",
                     Charset.forName("UTF-8"))
        }

      private def googleMapPreviewUrl(location: Location): Option[String] =
        encodedAddress(location).map { address =>
          Uri
            .fromString(
              s"https://maps.googleapis.com/maps/api/staticmap?center=$address&size=300x300&key=$apiKey")
            .valueOr(throw _)
            .renderString
        }

      private def googleMapLink(location: Location): Option[String] = {
        encodedAddress(location).map { address =>
          Uri
            .fromString(s"https://www.google.com/maps/search/$address")
            .valueOr(throw _)
            .renderString
        }
      }

      private def drivingDirectionsLink(location: Location): Option[String] = {
        encodedAddress(location).map { address =>
          Uri
            .fromString(s"https://www.google.com/maps/dir//$address")
            .valueOr(throw _)
            .renderString
        }
      }

      private def buttons(phones: List[Phone],
                          placeUrl: Option[String],
                          drivingUrl: Option[String]): List[Button] = {
        List(
          placeUrl.map(Button("url", _, "View on map")),
          drivingUrl.map(Button("url", _, "Driving directions")),
          phones.headOption.map(p =>
            Button("phone", p.number, "Make a phone call"))
        ).flatten
      }

      private def header(location: Location): String =
        s"${location.show}\n\n${location.addresses
          .map(_.show)
          .mkString("\n")}\n\n☎ ${location.phones.map(_.show).mkString("\n\n")}"

      private def schedule(schedule: List[DailySchedule]): Option[String] =
        Some(schedule.map(_.show).mkString("\n"))
    }
}
