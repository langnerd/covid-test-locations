package com.langnerd.covid.testlocations.api.postman

import cats.Applicative
import cats.effect.Sync
import io.circe.generic.semiauto._
import io.circe.{Decoder, Encoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}

final case class Location(id: String,
                          organization_id: String,
                          name: String,
                          alternate_name: String,
                          description: String,
                          transportation: String,
                          updated: String,
                          featured: String,
                          physical_address: List[PhysicalAddress],
                          phones: List[Phone],
                          regular_schedule: List[RegularSchedule])

object Location {

  implicit val locationDecoder: Decoder[Location] = deriveDecoder[Location]

  implicit def locationEntityDecoder[F[_]: Sync]
  : EntityDecoder[F, List[Location]] =
    jsonOf

  implicit val locationEncoder: Encoder[Location] = deriveEncoder[Location]

  implicit def locationEntityEncoder[F[_]: Applicative]
  : EntityEncoder[F, List[Location]] =
    jsonEncoderOf
}
