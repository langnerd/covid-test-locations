package com.langnerd.covid.testlocations.api.snatchbot

import com.langnerd.covid.testlocations.validation.Validator
import cats.Applicative
import cats.effect.Sync
import io.circe.generic.semiauto._
import io.circe.{Decoder, Encoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}


final case class ConversationId(user_id: String,
                                bot_id: String,
                                module_id: String)

object ConversationId {
  import cats.implicits._
  import com.langnerd.covid.testlocations.validation.FieldValidator.strings._

  implicit val validator: Validator[ConversationId] =
    (target: ConversationId) => {
      NonEmpty.validate(target.user_id, "user_id") |+|
        NonEmpty.validate(target.bot_id, "bot_id") |+|
        NonEmpty.validate(target.module_id, "module_id")
    }

  implicit val conversationIdDecoder: Decoder[ConversationId] = deriveDecoder[ConversationId]

  implicit def conversationIdEntityDecoder[F[_]: Sync]
  : EntityDecoder[F, ConversationId] =
    jsonOf

  implicit val conversationIdEncoder: Encoder[ConversationId] = deriveEncoder[ConversationId]

  implicit def conversationIdEntityEncoder[F[_]: Applicative]
  : EntityEncoder[F, ConversationId] =
    jsonEncoderOf
}
