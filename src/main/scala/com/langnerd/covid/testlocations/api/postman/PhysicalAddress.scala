package com.langnerd.covid.testlocations.api.postman

import cats.Applicative
import cats.effect.Sync
import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import org.http4s.{EntityDecoder, EntityEncoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}

final case class PhysicalAddress(id: String,
                                 location_id: String,
                                 address_1: String,
                                 city: String,
                                 region: String,
                                 state_province: String,
                                 postal_code: String,
                                 country: String)

object PhysicalAddress {

  implicit val physicalAddressDecoder: Decoder[PhysicalAddress] =
    deriveDecoder[PhysicalAddress]

  implicit def physicalAddressEntityDecoder[F[_]: Sync]
  : EntityDecoder[F, PhysicalAddress] =
    jsonOf

  implicit val physicalAddressEncoder: Encoder[PhysicalAddress] =
    deriveEncoder[PhysicalAddress]

  implicit def physicalAddressEntityEncoder[F[_]: Applicative]
  : EntityEncoder[F, PhysicalAddress] =
    jsonEncoderOf
}
