package com.langnerd.covid.testlocations.api.snatchbot

final case class SearchQuery(conversationId: ConversationId,
                             us_state: Option[String],
                             us_county: Option[String],
                             address: Option[String])
