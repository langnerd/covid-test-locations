package com.langnerd.covid.testlocations.api.postman

import cats.Applicative
import cats.effect.Sync
import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import org.http4s.{EntityDecoder, EntityEncoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}

final case class RegularSchedule(id: String, location_id: String, weekday: String, opens_at: String, closes_at: String)

object RegularSchedule {
  implicit val regularScheduleDecoder: Decoder[RegularSchedule] =
    deriveDecoder[RegularSchedule]

  implicit def regularScheduleEntityDecoder[F[_]: Sync]
  : EntityDecoder[F, RegularSchedule] =
    jsonOf

  implicit val regularScheduleEncoder: Encoder[RegularSchedule] =
    deriveEncoder[RegularSchedule]

  implicit def regularScheduleEntityEncoder[F[_]: Applicative]
  : EntityEncoder[F, RegularSchedule] =
    jsonEncoderOf
}
