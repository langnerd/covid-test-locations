package com.langnerd.covid.testlocations.api.postman

import cats.Applicative
import cats.effect.Sync
import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import org.http4s.{EntityDecoder, EntityEncoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}

final case class Phone(id: String,
                       location_id: String,
                       number: String,
                       extension: String,
                       `type`: String,
                       language: String,
                       description: String)

object Phone {

  implicit val phoneDecoder: Decoder[Phone] =
    deriveDecoder[Phone]

  implicit def phoneEntityDecoder[F[_]: Sync]: EntityDecoder[F, Phone] =
    jsonOf

  implicit val phoneEncoder: Encoder[Phone] =
    deriveEncoder[Phone]

  implicit def phoneEntityEncoder[F[_]: Applicative]: EntityEncoder[F, Phone] =
    jsonEncoderOf
}
