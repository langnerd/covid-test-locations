package com.langnerd.covid.testlocations

import cats.effect.{Async, Sync}
import cats.implicits._
import com.langnerd.covid.testlocations.api.snatchbot.ConversationId
import com.langnerd.covid.testlocations.model.Location
import com.langnerd.covid.testlocations.model.core.{CountyQuery, RegionCodes}
import com.langnerd.covid.testlocations.service.{
  LocationService,
  SnatchBotService
}
import io.circe.Encoder
import org.http4s.circe.jsonEncoderOf
import org.http4s.dsl.Http4sDsl
import org.http4s.dsl.impl.OptionalQueryParamDecoderMatcher
import org.http4s.{EntityEncoder, HttpRoutes, Response, Uri, UrlForm}
object Routes {

  object DisplayFormat
      extends OptionalQueryParamDecoderMatcher[String]("display_format")

  object SearchBy extends OptionalQueryParamDecoderMatcher[String]("by")

  def locationRoutes[F[_]: Sync](L: LocationService[F], S: SnatchBotService[F])(
      implicit F: Async[F]): HttpRoutes[F] = {

    val dsl = new Http4sDsl[F] {}
    import dsl._

    def usLocations(format: Option[String],
                    data: => F[List[Location]]): F[Response[F]] = {
      implicit val locationEncoder: Encoder[Location] =
        if (format.contains("flat"))
          Location.Encoders.conciseEncoder
        else
          Location.Encoders.defaultEncoder

      implicit def locationEntityEncoder: EntityEncoder[F, List[Location]] =
        jsonEncoderOf

      for {
        locations <- data
        res <- Ok(locations)
      } yield res
    }

    def usLocationsAsCards(conversationId: ConversationId,
                           region: String,
                           data: => F[List[Location]]): F[Response[F]] =
      ConversationId.validator.validate(conversationId) match {
        case None =>
          for {
            locations <- data
            cards <- S.cards(conversationId, region, locations)
            res <- Ok(cards)
          } yield res
        case Some(errors) =>
          BadRequest(errors.toList)
      }

    HttpRoutes.of[F] {
      case GET -> Root / "locations" / "us" / region :? DisplayFormat(format) =>
        usLocations(format, L.get(region))

      case GET -> Root / "locations" / "us" / region / county :? DisplayFormat(
            format) =>
        usLocations(format, L.get(region, county))

      case req @ POST -> Root / "locations" / "us" / "search" :? SearchBy(
            searchBy) =>
        req.decode[UrlForm] { form =>
          val conversationId = extractConversationId(form)
          val maybeQuery =
            if (searchBy.contains("address")) queryByAddress(form)
            else queryByCountryAndCounty(form)

          maybeQuery.map { query =>
            usLocationsAsCards(conversationId,
                               query.regionCode,
                               L.get(query.regionCode, query.county))
          } getOrElse {
            BadRequest("Invalid query")
          }
        }
      case req @ POST -> Root / "locations" / "us" / region =>
        req.decode[UrlForm] { data =>
          usLocationsAsCards(extractConversationId(data), region, L.get(region))
        }
      case req @ POST -> Root / "locations" / "us" / region / county =>
        req.decode[UrlForm] { data =>
          usLocationsAsCards(extractConversationId(data),
                             region,
                             L.get(region, county))
        }
    }
  }

  private def extractField(fieldName: String, form: UrlForm): String = {
    form.values.get(fieldName).flatMap(_.uncons) match {
      case Some((s, _)) => Uri.decode(s, plusIsSpace = true)
      case _            => ""
    }
  }

  private def queryByCountryAndCounty(form: UrlForm): Option[CountyQuery] = {
    val state = extractField("country", form)
    val county = extractField("incoming_message", form)
    val regionCode =
      if (state.isEmpty) None else RegionCodes.US.findRegionCode(state)

    if (regionCode.isEmpty || county.isEmpty) {
      None
    } else {
      Some(CountyQuery(regionCode.get, county))
    }
  }

  private def queryByAddress(form: UrlForm): Option[CountyQuery] = {
    val address = extractField("address", form)
    if (address.isEmpty) {
      None
    } else {
      val chunks = address.split(",")
      if (chunks.length < 3) None
      else {
        val pattern = "([A-Z]{2}) [0-9]{5,6}".r
        chunks(2).trim match {
          case pattern(regionCode) =>
            val county = chunks(1).trim
            Some(CountyQuery(regionCode, county))
          case _ => None
        }
      }
    }
  }

  private def extractConversationId(form: UrlForm): ConversationId =
    ConversationId(extractField("user_id", form),
                   extractField("bot_id", form),
                   extractField("module_id", form))

}
