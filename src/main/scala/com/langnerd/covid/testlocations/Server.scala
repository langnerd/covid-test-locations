package com.langnerd.covid.testlocations

import cats.effect.{ConcurrentEffect, ContextShift, Timer}
import com.langnerd.covid.testlocations.core.CastlightHealthWebScraper
import com.langnerd.covid.testlocations.datasource.impl.{
  CastlightHealthCountyProvider,
  PostmanCountryProvider
}
import com.langnerd.covid.testlocations.service.{
  LocationService,
  SnatchBotService
}
import fs2.Stream
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.implicits._
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.middleware.Logger

import scala.concurrent.ExecutionContext.global
import scala.util.Properties

object Server {

  def stream[F[_]: ConcurrentEffect](implicit T: Timer[F],
                                     C: ContextShift[F]): Stream[F, Nothing] = {
    Properties.envOrNone("GOOGLE_MAPS_API_KEY") match {
      case Some(apiKey) =>
        startServer(apiKey)
      case None =>
        startServer("AIzaSyCYpsLyNQp1RSebpolVhEJ6R_DmOIoaY9c")
      //        Stream.raiseError[F](
//          new IllegalStateException("Missing config: Google Maps API key"))
    }
  }.drain

  private def startServer[F[_]: ConcurrentEffect](
      apiKey: String)(implicit T: Timer[F], C: ContextShift[F]) = {
    for {
      client <- BlazeClientBuilder[F](global).stream
      webScraper = CastlightHealthWebScraper.impl[F]
      countyProvider = new CastlightHealthCountyProvider[F](client, webScraper)
      countryProvider = new PostmanCountryProvider[F](client)
      locationService = LocationService.impl[F](countyProvider, countryProvider)
      snatchBotService = SnatchBotService.impl[F](apiKey)

      // Combine Service Routes into an HttpApp.
      // Can also be done via a Router if you
      // want to extract a segments not checked
      // in the underlying routes.
      httpApp = Routes
        .locationRoutes[F](locationService, snatchBotService)
        .orNotFound

      // With Middlewares in place
      finalHttpApp = Logger.httpApp(logHeaders = true, logBody = true)(httpApp)

      port = Properties.envOrElse("PORT", "8081").toInt

      exitCode <- BlazeServerBuilder[F]
        .bindHttp(port, "0.0.0.0")
        .withHttpApp(finalHttpApp)
        .serve
    } yield exitCode
  }
}
