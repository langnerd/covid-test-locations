package com.langnerd.covid.testlocations.core

import java.util.UUID

import cats.Applicative
import cats.effect.Sync
import com.langnerd.covid.testlocations.model._
import com.langnerd.covid.testlocations.model.core.{HtmlDoc, RegionCodes}
import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.model.Element

import scala.util.Try

object CastlightHealthWebScraper {

  def apply[F[_]](implicit ev: WebScraper[F]): WebScraper[F] = ev

  def impl[F[_]: Sync](implicit F: Applicative[F]): WebScraper[F] =
    new WebScraper[F] {

      private val browser = JsoupBrowser()

      override def scrape(county: String, regionCode: String, htmlDoc: HtmlDoc): F[List[Location]] = {
        val maybeLocations: Option[List[Location]] = for {
          doc <- parseDoc(htmlDoc.html)
        } yield {
          val results = parseResults(doc)
          val region = RegionCodes.US.find(regionCode)
          results.flatMap(parseLocation(county, region, _))
        }
        F.pure(maybeLocations.getOrElse(List.empty))
      }

      private def parseDoc(url: String): Option[browser.DocumentType] = {
        Try(browser.parseString(url)).toOption
      }

      private def parseResults(doc: browser.DocumentType): List[Element] =
        doc >> elementList("body > div")

      private def parseLocation(county: String, region: Option[String], element: Element): Option[Location] = {
        for {
          name <- parseName(element)
          address <- parseAddress(county, region, element)
          phone <- parsePhone(element)
        } yield
          Location(locationId,
                   name,
                   parseDescription(element),
                   List(address),
                   List(phone),
                   List.empty)
      }

      private def parseName(element: Element): Option[String] =
        Try(element >> text("h2")).toOption

      private def parseAddress(county: String, region: Option[String], element: Element): Option[Address] =
        for {
          elements <- Try(element >> elementList("p")).toOption
          head <- elements.headOption
          addressLine <- Try(head >> text("a")).toOption
          address <- addressLine.split(",") match {
            case Array(_, street, city, postalCode) =>
              Some(Address(addressId, street, city, Some(county), region, postalCode, "us"))
            case Array(street, city, postalCode) =>
              Some(Address(addressId, street, city, Some(county), region, postalCode, "us"))
            case _ => None
          }
        } yield address

      private def parseDescription(element: Element): Option[String] =
        for {
          elements <- Try(element >> elementList("p")).toOption
          description <- elements match {
            case List(_, _, _, elm, _*) =>
              Some(elm.text).filter(_ != "Report an error")
            case _ => None
          }
        } yield description

      private def parsePhone(element: Element): Option[Phone] = {
        for {
          elements <- Try(element >> elementList("p")).toOption
          head <- elements.tail.headOption
          phoneNumber <- Try(head >> text("a") ).toOption
        } yield Phone(phoneNumber, "")
      }

      private def locationId: LocationId =
        LocationId(UUID.randomUUID().toString)

      private def addressId: AddressId =
        AddressId(UUID.randomUUID().toString)
    }
}
