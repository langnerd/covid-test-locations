package com.langnerd.covid.testlocations.core

import com.langnerd.covid.testlocations.model.Location
import com.langnerd.covid.testlocations.model.core.HtmlDoc

trait WebScraper[F[_]] {
  def scrape(county: String, regionCode: String, htmlDoc: HtmlDoc): F[List[Location]]
}
