package com.langnerd.covid.testlocations.datasource.impl

import cats.effect.{Concurrent, Sync}
import cats.syntax.all._
import com.langnerd.covid.testlocations.core.WebScraper
import com.langnerd.covid.testlocations.datasource.CountyProvider
import com.langnerd.covid.testlocations.model.core.{CountyQuery, HtmlDoc}
import com.langnerd.covid.testlocations.model.{ApiError, Location}
import org.http4s.Method.GET
import org.http4s.Uri
import org.http4s.client.Client
import org.http4s.client.dsl.Http4sClientDsl

import scala.util.{Failure, Success, Try}

/**
  * All US states, county level, web scraped.
  * Credit: https://my.castlighthealth.com/corona-virus-testing-sites/
  */
class CastlightHealthCountyProvider[F[_]: Sync](
    client: Client[F],
    webScraper: WebScraper[F])(implicit F: Concurrent[F])
    extends CountyProvider[F] {

  private val dsl = new Http4sClientDsl[F] {}
  import CastlightHealthCountyProvider.BaseUrl
  import dsl._

  override def find(query: CountyQuery): F[List[Location]] = {
    Try(Uri.encode(
      s"$BaseUrl?county=${query.county}&state=${query.regionCode}")) match {
      case Success(url) =>
        F.fromEither(Uri.fromString(url))
          .flatMap { uri =>
            for {
              html <- client.expect[String](GET(uri)).adaptError {
                case t => ApiError(t)
              }
              result <- webScraper.scrape(query.county,
                                          query.regionCode,
                                          HtmlDoc(html))
            } yield result
          }
      case Failure(exception) =>
        F.raiseError(exception)
    }
  }
}

object CastlightHealthCountyProvider {
  final val BaseUrl: String =
    "https://my.castlighthealth.com/corona-virus-testing-sites/data/result.php"
}
