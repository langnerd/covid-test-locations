package com.langnerd.covid.testlocations.datasource.impl

import cats.effect.{Concurrent, Sync}
import cats.syntax.all._
import com.langnerd.covid.testlocations.api.postman.{
  Location => ApiLocation,
  Phone => ApiPhone,
  PhysicalAddress => ApiAddress,
  RegularSchedule => ApiSchedule
}
import com.langnerd.covid.testlocations.datasource.CountryProvider
import com.langnerd.covid.testlocations.model._
import com.langnerd.covid.testlocations.model.core.{RegionCodes, RegionQuery}
import org.http4s.Method.GET
import org.http4s.Uri
import org.http4s.client.Client
import org.http4s.client.dsl.Http4sClientDsl

/**
  * Selected US states only, API based.
  * Credit: https://covid-19-apis.postman.com/covid-19-testing-locations/
  */
class PostmanCountryProvider[F[_]: Sync](client: Client[F])(
    implicit F: Concurrent[F])
    extends CountryProvider[F] {

  private val dsl = new Http4sClientDsl[F] {}
  import dsl._

  override def find(query: RegionQuery): F[List[Location]] = {
    RegionCodes.US.find(query.regionCode).map { region =>
      F.fromEither(Uri.fromString(
          s"https://covid-19-testing.github.io/locations/${formatRegion(region)}/complete.json"))
        .flatMap { uri =>
          client
            .expect[List[ApiLocation]](GET(uri))
            .adaptError { case t => ApiError(t) } // Prevent Client Json Decoding Failure Leaking
            .map(_.map(toModel(region, _)))
        }
    } getOrElse F.pure(List.empty)
  }

  private def formatRegion(region: String): String =
    region.toLowerCase.replaceAll(" ", "-")

  private def toModel(region: String, api: ApiLocation): Location =
    Location(LocationId(api.id),
             api.name,
             Option(api.description),
             api.physical_address.map(address(region, _)),
             api.phones.map(phone),
             api.regular_schedule.flatMap(schedule))

  private def address(region: String, api: ApiAddress): Address =
    Address(AddressId(api.id),
            api.address_1,
            api.city,
            Option(api.region),
            Option(region),
            api.postal_code,
            api.country)

  private def phone(api: ApiPhone): Phone =
    Phone(api.number, api.extension)

  private def schedule(api: ApiSchedule): Option[DailySchedule] =
    for {
      day <- weekday(api.weekday)
      opensAt <- timeOfDay(api.opens_at)
      closesAt <- timeOfDay(api.closes_at)
    } yield DailySchedule(day, opensAt, closesAt)

  private def weekday(value: String): Option[WeekDay] = {
    try {
      val index = value.toInt
      if (index >= 1 && index <= 7) Some(WeekDay(index)) else None
    } catch {
      case _: Exception => None
    }
  }

  private def timeOfDay(value: String): Option[String] = {
    val pattern =
      "([0-9]|0[0-9]|1[0-9]|2[0-3]):([0-5][0-9])\\s*([AaPp][Mm])".r
    Some(value).filter(pattern.matches)
  }
}
