package com.langnerd.covid.testlocations.datasource

import com.langnerd.covid.testlocations.model.Location
import com.langnerd.covid.testlocations.model.core.{RegionQuery, CountyQuery, LocationQuery}

sealed trait LocationProvider[F[_], A <: LocationQuery] {
  def find(query: A): F[List[Location]]
}

trait CountryProvider[F[_]] extends LocationProvider[F, RegionQuery]

trait CountyProvider[F[_]] extends LocationProvider[F, CountyQuery]