package com.langnerd.covid.testlocations.validation

import cats.data.NonEmptyList
import cats.implicits._

trait FieldValidator[T] {

  def validate(field: T, fieldName: FieldName): ValidationResult
}

object FieldValidator {
  object strings {
    case object NonEmpty extends FieldValidator[String] {
      override def validate(field: String, fieldName: FieldName): ValidationResult =
        if (field.isEmpty)
          NonEmptyList.of(FieldError(fieldName, "Must not be empty")).some
        else None
    }
  }
}
