package com.langnerd.covid.testlocations.validation

trait Validator[T] {

  def validate(target: T): ValidationResult
}
