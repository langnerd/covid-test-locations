package com.langnerd.covid.testlocations

import cats.Applicative
import cats.data.NonEmptyList
import cats.effect.Sync

// credit: http://fruzenshtein.com/http4s-json-request-validator/
package object validation {

  type FieldName = String

  type Message = String

  type ValidationResult = Option[NonEmptyList[FieldError]]

  final case class FieldError(fieldName: FieldName, message: Message)

  object FieldError {
    import io.circe.{Decoder, Encoder}
    import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
    import org.http4s.{EntityDecoder, EntityEncoder}
    import org.http4s.circe.{jsonEncoderOf, jsonOf}

    implicit val fieldErrorDecoder: Decoder[FieldError] =
      deriveDecoder[FieldError]

    implicit def fieldErrorEntityDecoder[F[_]: Sync]
      : EntityDecoder[F, List[FieldError]] =
      jsonOf

    implicit val fieldErrorEncoder: Encoder[FieldError] =
      deriveEncoder[FieldError]

    implicit def fieldErrorEntityEncoder[F[_]: Applicative]
      : EntityEncoder[F, List[FieldError]] = jsonEncoderOf
  }
}
