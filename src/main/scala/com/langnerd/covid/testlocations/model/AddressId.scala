package com.langnerd.covid.testlocations.model

import cats.Applicative
import cats.effect.Sync
import io.circe.generic.semiauto._
import io.circe.{Decoder, Encoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}

final case class AddressId(value: String)

object AddressId {
  implicit val addressIdDecoder: Decoder[AddressId] = deriveDecoder[AddressId]

  implicit def addressIdEntityDecoder[F[_]: Sync]
  : EntityDecoder[F, AddressId] =
    jsonOf

  implicit val addressIdEncoder: Encoder[AddressId] = deriveEncoder[AddressId]

  implicit def addressIdEntityEncoder[F[_]: Applicative]
  : EntityEncoder[F, AddressId] =
    jsonEncoderOf
}