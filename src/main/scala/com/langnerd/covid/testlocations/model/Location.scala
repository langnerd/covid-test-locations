package com.langnerd.covid.testlocations.model

import cats.Show
import cats.effect.Sync
import io.circe.generic.semiauto._
import io.circe.literal._
import io.circe.{Decoder, Encoder}
import org.http4s.EntityDecoder
import org.http4s.circe.jsonOf

final case class Location(id: LocationId,
                          name: String,
                          description: Option[String],
                          addresses: List[Address],
                          phones: List[Phone],
                          schedule: List[DailySchedule])

object Location {
  implicit val show: Show[Location] = _.name

  implicit val locationDecoder: Decoder[Location] = deriveDecoder[Location]

  implicit def locationEntityDecoder[F[_]: Sync]
    : EntityDecoder[F, List[Location]] =
    jsonOf

  object Encoders {
    val defaultEncoder: Encoder[Location] = deriveEncoder[Location]

    val conciseEncoder: Encoder[Location] = Encoder.instance {
      location: Location =>
        val description =
          s"${location.name}${address(location.addresses)}${phone(location.phones)}".trim
        json"""$description"""
    }

    private def phone(phones: List[Phone]): String =
      phones.headOption.map(p => s" | ${p.number.trim}").getOrElse("")

    private def address(addresses: List[Address]): String =
      addresses.headOption
        .map(a => s" | ${a.street} - ${a.city} - ${a.postalCode}")
        .getOrElse("")
  }
}
