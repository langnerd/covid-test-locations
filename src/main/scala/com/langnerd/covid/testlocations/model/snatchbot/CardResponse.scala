package com.langnerd.covid.testlocations.model.snatchbot

import cats.Applicative
import cats.effect.Sync
import io.circe.generic.semiauto._
import io.circe.{Decoder, Encoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}

final case class CardResponse(user_id: String,
                              bot_id: String,
                              module_id: String,
                              message: String,
                              cards: List[GalleryCard])

object CardResponse {
  implicit val cardResponseDecoder: Decoder[CardResponse] = deriveDecoder[CardResponse]

  implicit def cardResponseEntityDecoder[F[_]: Sync]
  : EntityDecoder[F, CardResponse] =
    jsonOf

  implicit val cardResponseEncoder: Encoder[CardResponse] = deriveEncoder[CardResponse]

  implicit def cardResponseEntityEncoder[F[_]: Applicative]
  : EntityEncoder[F, CardResponse] =
    jsonEncoderOf
}