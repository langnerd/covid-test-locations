package com.langnerd.covid.testlocations.model

import cats.{Applicative, Show}
import cats.effect.Sync
import cats.implicits._
import io.circe.generic.semiauto._
import io.circe.{Decoder, Encoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}

final case class DailySchedule(weekDay: WeekDay, opensAt: String, closesAt: String)

object DailySchedule {
  implicit val dailyScheduleDecoder: Decoder[DailySchedule] = deriveDecoder[DailySchedule]

  implicit val show: Show[DailySchedule] = { schedule =>
    s"${schedule.weekDay.show}:\t${schedule.opensAt} - ${schedule.closesAt}"
  }

  implicit def dailyScheduleEntityDecoder[F[_]: Sync]
  : EntityDecoder[F, DailySchedule] =
    jsonOf

  implicit val dailyScheduleEncoder: Encoder[DailySchedule] = deriveEncoder[DailySchedule]

  implicit def dailyScheduleEntityEncoder[F[_]: Applicative]
  : EntityEncoder[F, List[DailySchedule]] =
    jsonEncoderOf
}