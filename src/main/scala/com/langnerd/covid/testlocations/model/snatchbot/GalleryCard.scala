package com.langnerd.covid.testlocations.model.snatchbot
import cats.Applicative
import cats.effect.Sync
import io.circe.generic.semiauto._
import io.circe.{Decoder, Encoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}


final case class GalleryCard(`type`: String, value: String, gallery: List[GalleryItem])

object GalleryCard {
  implicit val galleryCardDecoder: Decoder[GalleryCard] =
    deriveDecoder[GalleryCard]

  implicit def galleryCardEntityDecoder[F[_]: Sync]
    : EntityDecoder[F, List[GalleryCard]] =
    jsonOf

  implicit val galleryCardEncoder: Encoder[GalleryCard] =
    deriveEncoder[GalleryCard]

  implicit def galleryCardEntityEncoder[F[_]: Applicative]
    : EntityEncoder[F, List[GalleryCard]] =
    jsonEncoderOf
}
