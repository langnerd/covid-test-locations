package com.langnerd.covid.testlocations.model

import cats.Applicative
import cats.effect.Sync
import io.circe.generic.semiauto._
import io.circe.{Decoder, Encoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}

final case class LocationId(value: String)

object LocationId {
  implicit val locationIdDecoder: Decoder[LocationId] = deriveDecoder[LocationId]

  implicit def locationIdEntityDecoder[F[_]: Sync]
  : EntityDecoder[F, LocationId] =
    jsonOf

  implicit val locationIdsEncoder: Encoder[LocationId] = deriveEncoder[LocationId]

  implicit def locationIdEntityEncoder[F[_]: Applicative]
  : EntityEncoder[F, List[LocationId]] =
    jsonEncoderOf
}
