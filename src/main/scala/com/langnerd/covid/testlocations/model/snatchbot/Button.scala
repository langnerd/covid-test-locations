package com.langnerd.covid.testlocations.model.snatchbot

import cats.Applicative
import cats.effect.Sync
import io.circe.generic.semiauto._
import io.circe.{Decoder, Encoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}

final case class Button(`type`: String, value: String, name: String)

object Button {
  implicit val buttonDecoder: Decoder[Button] = deriveDecoder[Button]

  implicit def buttonEntityDecoder[F[_]: Sync]
  : EntityDecoder[F, List[Button]] =
    jsonOf

  implicit val buttonEncoder: Encoder[Button] = deriveEncoder[Button]

  implicit def buttonEntityEncoder[F[_]: Applicative]
  : EntityEncoder[F, List[Button]] =
    jsonEncoderOf
}