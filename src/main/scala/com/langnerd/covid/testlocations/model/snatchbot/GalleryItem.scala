package com.langnerd.covid.testlocations.model.snatchbot

import cats.Applicative
import cats.effect.Sync
import io.circe.generic.semiauto._
import io.circe.{Decoder, Encoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}

final case class GalleryItem(image: Option[String],
                             heading: String,
                             subtitle: Option[String],
                             url: Option[String],
                             buttons: List[Button])

object GalleryItem {
  implicit val galleryItemDecoder: Decoder[GalleryItem] = deriveDecoder[GalleryItem]

  implicit def galleryItemEntityDecoder[F[_]: Sync]
  : EntityDecoder[F, List[GalleryItem]] =
    jsonOf

  implicit val galleryItemEncoder: Encoder[GalleryItem] = deriveEncoder[GalleryItem]

  implicit def galleryItemEntityEncoder[F[_]: Applicative]
  : EntityEncoder[F, List[GalleryItem]] =
    jsonEncoderOf
}