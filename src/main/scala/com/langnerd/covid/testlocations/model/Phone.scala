package com.langnerd.covid.testlocations.model

import cats.{Applicative, Show}
import cats.effect.Sync
import io.circe.generic.semiauto._
import io.circe.{Decoder, Encoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}

final case class Phone(number: String, extension: String)

object Phone {
  implicit val show: Show[Phone] = _.number

  implicit val phoneDecoder: Decoder[Phone] = deriveDecoder[Phone]

  implicit def phoneEntityDecoder[F[_]: Sync]
  : EntityDecoder[F, Phone] =
    jsonOf

  implicit val phoneEncoder: Encoder[Phone] = deriveEncoder[Phone]

  implicit def phoneEntityEncoder[F[_]: Applicative]
  : EntityEncoder[F, List[Phone]] =
    jsonEncoderOf
}