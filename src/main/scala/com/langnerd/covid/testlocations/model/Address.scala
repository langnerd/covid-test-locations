package com.langnerd.covid.testlocations.model

import cats.{Applicative, Show}
import cats.effect.Sync
import com.langnerd.covid.testlocations.model.core.Constants
import io.circe.generic.semiauto._
import io.circe.{Decoder, Encoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}

final case class Address(id: AddressId,
                         street: String,
                         city: String,
                         county: Option[String],
                         region: Option[String],
                         postalCode: String,
                         country: String)

object Address {
  implicit val show: Show[Address] = { address =>
    val county = if (isEmpty(address.county)) Constants.Strings.Empty else address.county.get
    val region = if (isEmpty(address.region)) Constants.Strings.Empty else address.region.get

    if (county.isEmpty && region.isEmpty) {
      s"${address.street}, ${address.city}, ${address.postalCode}, ${address.country}"
    } else if (county.isEmpty) {
      s"${address.street}, ${address.city}, ${address.postalCode}, ${region}, ${address.country}"
    } else if (region.isEmpty) {
      s"${address.street}, ${address.city}, ${county}, ${address.postalCode}, ${address.country}"
    } else {
      s"${address.street}, ${address.city}, ${county}, ${address.postalCode}, ${region}, ${address.country}"
    }
  }

  implicit val addressDecoder: Decoder[Address] = deriveDecoder[Address]

  implicit def addressEntityDecoder[F[_]: Sync]: EntityDecoder[F, Address] =
    jsonOf

  implicit val addressEncoder: Encoder[Address] = deriveEncoder[Address]

  implicit def addressEntityEncoder[F[_]: Applicative]
    : EntityEncoder[F, List[Address]] =
    jsonEncoderOf

  private def isEmpty(value: Option[String]): Boolean =
    value.isEmpty || value.exists(_.toUpperCase == Constants.Strings.None)
}
