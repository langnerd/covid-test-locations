package com.langnerd.covid.testlocations.model.core

final case class HtmlDoc(html: String)
