package com.langnerd.covid.testlocations.model.core

sealed trait LocationQuery

final case class RegionQuery(regionCode: String) extends LocationQuery

final case class CountyQuery(regionCode: String, county: String)
    extends LocationQuery
