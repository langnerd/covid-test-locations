package com.langnerd.covid.testlocations.model

import cats.{Applicative, Show}
import cats.effect.Sync
import io.circe.generic.semiauto._
import io.circe.{Decoder, Encoder}
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.{EntityDecoder, EntityEncoder}

final case class WeekDay(ordinal: Int)

object WeekDay {

  implicit val show: Show[WeekDay] = _.ordinal match {
    case 1 => "Sun"
    case 2 => "Mon"
    case 3 => "Tue"
    case 4 => "Wed"
    case 5 => "Thu"
    case 6 => "Fri"
    case 7 => "Sat"
    case _ => "undefined"
  }

  implicit val weekDayDecoder: Decoder[WeekDay] = deriveDecoder[WeekDay]

  implicit def weekDayEntityDecoder[F[_]: Sync]
  : EntityDecoder[F, WeekDay] =
    jsonOf

  implicit val weekDayEncoder: Encoder[WeekDay] = deriveEncoder[WeekDay]

  implicit def weekDayEntityEncoder[F[_]: Applicative]
  : EntityEncoder[F, List[WeekDay]] =
    jsonEncoderOf
}
