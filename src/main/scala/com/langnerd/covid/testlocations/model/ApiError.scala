package com.langnerd.covid.testlocations.model

final case class ApiError(e: Throwable) extends RuntimeException
