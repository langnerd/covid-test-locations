package com.langnerd.covid.testlocations.model.core

object Constants {

  object Strings {
    final val None: String = "NONE"
    final val Empty: String = ""
  }
}
